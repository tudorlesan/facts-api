package com.randomfacts.facts.utils;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Fact not downloaded.")
public class RandomFactNotFoundException extends Exception {
    public RandomFactNotFoundException() {
        super("Fact not downloaded.");
    }
}
