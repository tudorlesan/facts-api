package com.randomfacts.facts.utils;

import com.randomfacts.facts.domain.enums.Language;
import org.springframework.core.convert.converter.Converter;

public class LanguageEnumConverter implements Converter<String, Language> {

    @Override
    public Language convert(String code) {
        try {
            return Language.findByCode(code);
        } catch(Exception e) {
            return null;
        }
    }
}
