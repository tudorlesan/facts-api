package com.randomfacts.facts.domain;

public class FactTotal {
    private int total;
    private int unique;

    public FactTotal(int total, int unique) {
        this.total = total;
        this.unique = unique;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getUnique() {
        return unique;
    }

    public void setUnique(int unique) {
        this.unique = unique;
    }
}
