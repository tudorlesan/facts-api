package com.randomfacts.facts.domain.enums;

public enum DownloadStatus {
    COMPLETED,
    IN_PROGRESS,
    READY_TO_START,
    ERROR_OCCURRED
}
