package com.randomfacts.facts.domain.enums;


import com.fasterxml.jackson.annotation.JsonValue;

import java.util.stream.Stream;

public enum Language {
    ENGLISH("english", "en"),
    GERMAN("german", "de"),
    FRENCH("french", "fr"),
    ITALIAN("italian", "it"),
    SPANISH("spanish", "es"),
    RUSSIAN("russian", "ru");

    //    @JsonIgnore
    private String text;
    private String code;

    Language(String text, String code) {
        this.text = text;
        this.code = code;
    }

    public static Language findByCode(String code) {
        return Stream.of(values()).filter(language -> language.getCode().equalsIgnoreCase(code)).findFirst().orElse(null);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @JsonValue
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}
