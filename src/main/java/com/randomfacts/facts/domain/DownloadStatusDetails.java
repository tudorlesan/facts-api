package com.randomfacts.facts.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.randomfacts.facts.domain.enums.DownloadStatus;

import java.util.Objects;

@JsonPropertyOrder({"status", "factTotal"})
public class DownloadStatusDetails {

    @JsonProperty("status")
    private DownloadStatus downloadStatus;
    @JsonProperty("facts")
    private FactTotal factTotal;

    public DownloadStatusDetails() {
        this.downloadStatus = DownloadStatus.READY_TO_START;
        this.factTotal = new FactTotal(0, 0);
    }

    public DownloadStatusDetails(DownloadStatus downloadStatus, FactTotal factTotal) {
        this.downloadStatus = downloadStatus;
        this.factTotal = factTotal;
    }


    public DownloadStatus getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(DownloadStatus status) {
        this.downloadStatus = status;
    }

    public FactTotal getFactTotal() {
        return factTotal;
    }

    public void setFactTotal(FactTotal factTotal) {
        this.factTotal = factTotal;
    }

    public void incrementTotal() {
        this.getFactTotal().setTotal(getFactTotal().getTotal() + 1);
    }

    public void incrementUnique() {
        this.factTotal.setUnique(factTotal.getUnique() + 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DownloadStatusDetails that = (DownloadStatusDetails) o;
        return downloadStatus == that.downloadStatus &&
                Objects.equals(factTotal, that.factTotal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(downloadStatus, factTotal);
    }

    @Override
    public String toString() {
        return "DownloadStatusDetails{" +
                "downloadStatus=" + downloadStatus +
                ", factTotal=" + factTotal +
                '}';
    }
}
