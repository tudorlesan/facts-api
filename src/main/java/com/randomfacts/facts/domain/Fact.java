package com.randomfacts.facts.domain;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.randomfacts.facts.domain.enums.Language;

import java.util.Objects;

public class Fact {

    private String id;
    private String text;
    private String url;
    private String language;

    public Fact() {
    }

    @JsonCreator
    public Fact(@JsonProperty("id") String id, @JsonProperty("text") String text, @JsonProperty("permalink") String url, @JsonProperty("language") Language language) {
        this.id = id;
        this.text = text;
        this.url = url;
        this.language = language.getCode();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language.getCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fact that = (Fact) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(text, that.text) &&
                Objects.equals(url, that.url) &&
                language.equals(that.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, url, language);
    }

    @Override
    public String toString() {
        return "Fact{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", url='" + url + '\'' +
                ", language=" + language +
                '}';
    }
}
