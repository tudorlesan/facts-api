package com.randomfacts.facts.configuration;

import com.randomfacts.facts.domain.DownloadStatusDetails;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class FactsConfiguration {

    @Bean
    public static DownloadStatusDetails getDownloadStatusDetails() {
        return new DownloadStatusDetails();
    }

}
