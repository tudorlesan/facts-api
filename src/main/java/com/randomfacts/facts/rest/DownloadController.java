package com.randomfacts.facts.rest;


import com.randomfacts.facts.domain.DownloadStatusDetails;
import com.randomfacts.facts.service.FactDownloadService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
class DownloadController {

    private Logger LOGGER = LoggerFactory.getLogger(DownloadController.class);

    @Autowired
    private FactDownloadService factDownloadService;

    @ApiOperation(value = "Retrieves information regarding the current download process.")
    @GetMapping("/status")
    DownloadStatusDetails getDownloadStatusDetails() {
        LOGGER.debug("[ DownloadController.getDownloadStatusDetails() ] - was called");
        return factDownloadService.getDownloadStatusDetails();
    }
}
