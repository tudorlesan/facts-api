package com.randomfacts.facts.rest;


import com.randomfacts.facts.domain.Fact;
import com.randomfacts.facts.domain.enums.Language;
import com.randomfacts.facts.service.FactService;
import com.randomfacts.facts.utils.RandomFactNotFoundException;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/facts")
class FactController {

    private Logger LOGGER = LoggerFactory.getLogger(FactController.class);

    @Autowired
    private FactService factService;

    @ApiOperation(value = "Retrieves an array of IDs corresponding to all facts that were downloaded until now.")
    @GetMapping()
    ResponseEntity<List<String>> getAllRandomFactIds() {
        LOGGER.debug("[ FactController.getAllRandomFactIds() ] - was called");
        return new ResponseEntity<>(factService.getAllIds(), HttpStatus.OK);
    }

    @ApiOperation(value = "Retrieves information corresponding to a specified fact ID. If fact was not downloaded or not in the cache anymore, throws RandomFactNotFoundException")
    @GetMapping("/{id}")
    Fact getRandomFactById(@PathVariable String id, @RequestParam(defaultValue = "") Language lang) throws RandomFactNotFoundException {
        LOGGER.debug("[ FactController.getRandomFactById(" + id + ") ] - was called");
        if (lang != null) {
            return factService.getById(id, lang);
        }
        return factService.getById(id);
    }
}
