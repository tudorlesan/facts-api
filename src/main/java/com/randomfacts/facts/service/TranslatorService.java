package com.randomfacts.facts.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

@Service
public class TranslatorService {

    private static String YANDEX_API_KEY;

    private static Logger LOGGER = LoggerFactory.getLogger(FactDownloadService.class);

    static String translate(String text, String sourceLang, String targetLang) throws IOException {
        String urlString = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + YANDEX_API_KEY + "&text=" + replaceSpaces(text) + "&lang=" + sourceLang + "-" + targetLang + "";
        String response = request(urlString);
        LOGGER.debug("Request to translate: [" + urlString + "]");

        return response.substring(response.indexOf("text") + 8, response.length() - 3);
    }

    private static String replaceSpaces(String text) {
        if (!text.contains(" ")) {
            return text;
        }
        StringBuilder urlifiedText = new StringBuilder();
        for (char currentChar : text.trim().toCharArray()) {

            if (currentChar == ' ') {
                urlifiedText.append("%20");
            } else {
                urlifiedText.append(currentChar);
            }
        }

        return urlifiedText.toString();
    }


    private static String request(String urlString) throws IOException {
        URL url = new URL(urlString);
        URLConnection urlConn = url.openConnection();
        urlConn.addRequestProperty("User-Agent", "Mozilla");

        InputStream inStream = urlConn.getInputStream();

        String received = new BufferedReader(new InputStreamReader(inStream)).readLine();

        inStream.close();
        return received;
    }

    @Value("${yandex.api.key}")
    public void setAPIKey(String apiKey) {
        YANDEX_API_KEY = apiKey;
    }

}
