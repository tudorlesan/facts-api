package com.randomfacts.facts.service;


import com.randomfacts.facts.domain.Fact;
import com.randomfacts.facts.domain.enums.DownloadStatus;
import com.randomfacts.facts.domain.DownloadStatusDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.IntStream;

@Service
public class FactDownloadService {

    private Logger LOGGER = LoggerFactory.getLogger(FactDownloadService.class);

    @Value("${download.threadpool.size:1}")   // try with totalnumber of available cores
    private int threadPoolSize;

    private Map<String, Fact> idToDownload;
    private int failedFacts = 0;

    private RestTemplate restTemplate;
    private DownloadStatusDetails downloadStatusDetails;

    private ExecutorService downloadExecutor;
    private CompletionService<Fact> downloadCompletionService;

    @Autowired
    public FactDownloadService(RestTemplate restTemplate, DownloadStatusDetails downloadStatusDetails) {
        this.restTemplate = restTemplate;
        idToDownload = new HashMap<>();
        this.downloadStatusDetails = downloadStatusDetails;
    }

    @PostConstruct
    public void init() {
        this.downloadExecutor = Executors.newFixedThreadPool(threadPoolSize);
        this.downloadCompletionService = new ExecutorCompletionService<>(downloadExecutor);
    }


    void start(String endpointUrl, int nrOfDownloads) {
//        int cores = Runtime.getRuntime().availableProcessors();
        if (this.downloadStatusDetails.getDownloadStatus() == DownloadStatus.IN_PROGRESS) {
            LOGGER.info("Download still in progress. No new request will be taken at this time.");
            return;
        }

        LOGGER.info("Download started. Requesting [" + nrOfDownloads + "] objects to [" + endpointUrl + "]. Stand by for results...");
        this.downloadStatusDetails = new DownloadStatusDetails();

        StopWatch stopwatch = new StopWatch();
        stopwatch.start();

        IntStream.rangeClosed(1, nrOfDownloads).boxed()
                .forEach(call -> downloadCompletionService.submit(() -> restTemplate.getForObject(endpointUrl, Fact.class)));

        retrieveDownloadedObjects(nrOfDownloads);

        stopwatch.stop();
        LOGGER.info("Downloaded [" + getDownloadStatusDetails().getFactTotal().getTotal() + "] objects in [" + stopwatch.getTotalTimeMillis() + "] milliseconds out of which ["
                + getDownloadStatusDetails().getFactTotal().getUnique() + "] were unique. There were [" + failedFacts + "] failed requests.");

        downloadExecutor.shutdown();
        try {
            final boolean done = downloadExecutor.awaitTermination(10, TimeUnit.MINUTES);
            LOGGER.debug("FactDownloadService successful showdown - {}", done);
        } catch (InterruptedException e) {
            LOGGER.error("Exception awaiting termination.", e);
        }

    }

    private void retrieveDownloadedObjects(int nrOfDownloads) {
        for (int i = 0; i < nrOfDownloads; i++) {
            try {
                addToCache(downloadCompletionService.take().get());
                if (getTotalDownloads() == nrOfDownloads) {
                    if (failedFacts != 0) {
                        this.downloadStatusDetails.setDownloadStatus(DownloadStatus.ERROR_OCCURRED);
                    } else {
                        this.downloadStatusDetails.setDownloadStatus(DownloadStatus.COMPLETED);
                    }
                    break;
                }
            } catch (InterruptedException e) {
                LOGGER.error("Exception while retrieving objects.", e);
            } catch (ExecutionException e) {
                LOGGER.error(e.getMessage(), e);
                failedFacts++;
            }
        }
    }

    private void addToCache(Fact fact) {
        boolean inCache = false;
        if (idToDownload.containsKey(fact.getId())) {
            inCache = true;
        }
        idToDownload.put(fact.getId(), fact);
        LOGGER.debug("Added fact with id - [" + fact.getId() + "] to cache.");
        updateDownloadStatus(inCache);
    }

    private int getTotalDownloads() {
        return this.downloadStatusDetails.getFactTotal().getTotal();
    }

    private void updateDownloadStatus(boolean inCache) {
        if (!inCache) {
            this.downloadStatusDetails.incrementUnique();
        }
        this.downloadStatusDetails.incrementTotal();
    }

    Fact getById(String id) {
        return idToDownload.get(id);
    }

    //flush cache every hour
    @Scheduled(fixedRate = 60 * 60 * 1000)
    private void deleteAll() {
        if (this.downloadStatusDetails.getDownloadStatus() == DownloadStatus.IN_PROGRESS) {
            return;
        }
        idToDownload.clear();
        this.downloadStatusDetails.setDownloadStatus(DownloadStatus.READY_TO_START);
    }

    List<Fact> getAll() {
        return new ArrayList<>(idToDownload.values());
    }

    public DownloadStatusDetails getDownloadStatusDetails() {
        return downloadStatusDetails;
    }
}
