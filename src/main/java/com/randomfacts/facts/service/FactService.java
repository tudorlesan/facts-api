package com.randomfacts.facts.service;

import com.randomfacts.facts.domain.Fact;
import com.randomfacts.facts.domain.enums.DownloadStatus;
import com.randomfacts.facts.domain.enums.Language;
import com.randomfacts.facts.utils.RandomFactNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FactService {

    private Logger LOGGER = LoggerFactory.getLogger(FactService.class);

    private FactDownloadService factDownloadService;

    @Value("${download.endpoint}")
    private String downloadEndpoint;

    @Value("${download.total}")
    private int downloadTotal;

    @Autowired
    public FactService(FactDownloadService factDownloadService) {
        this.factDownloadService = factDownloadService;
    }

    public List<String> getAllIds() {
        if (factDownloadService.getDownloadStatusDetails().getDownloadStatus() != DownloadStatus.IN_PROGRESS) {
            factDownloadService.start(downloadEndpoint, downloadTotal);
        }
        List<String> result = factDownloadService.getAll().stream().map(Fact::getId).collect(Collectors.toList());

        LOGGER.debug("Returning [" + result.size() + "] facts.");
        return factDownloadService.getAll().stream().map(Fact::getId).collect(Collectors.toList());
    }

    public Fact getById(String id) throws RandomFactNotFoundException {
        Fact fact = factDownloadService.getById(id);
        if (fact == null) {
            throw new RandomFactNotFoundException();
        } else {
            LOGGER.debug("Returning fact with ID [" + fact.getId() +"].");
            return fact;
        }
    }

    public Fact getById(String id, Language language) throws RandomFactNotFoundException {
        Fact fact = getById(id);
        if(language == null || fact.getLanguage().equalsIgnoreCase(language.getCode())){
            return fact;
        }
        try {
            String translatedText = TranslatorService.translate(fact.getText(), fact.getLanguage(), language.getCode());
            return new Fact(fact.getId(), translatedText, fact.getUrl(), language);
        } catch (IOException e) {
            LOGGER.warn("Could not translate [ " + fact.getText() + "] into [ " + language.getText() + "]. " +
                    "Returning fact in original language.", e);
            return fact;
        }
    }

}
