package com.randomfacts.facts.rest;

import com.randomfacts.facts.TestConfig;
import com.randomfacts.facts.domain.Fact;
import com.randomfacts.facts.factory.FactTestDataFactory;
import com.randomfacts.facts.service.FactDownloadService;
import com.randomfacts.facts.service.FactService;
import com.randomfacts.facts.utils.RandomFactNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest
@Import(TestConfig.class)
class FactControllerTest {

    @Autowired
    private
    MockMvc mockMvc;

    @MockBean
    private FactService factService;

    @MockBean
    private FactDownloadService factDownloadService;

    private List<String> factIdList = FactTestDataFactory.getFactList().stream().map(Fact::getId).collect(Collectors.toList());

    @Test
    @WithMockUser
    void getAllRandomFactIds() throws Exception {
        when(factService.getAllIds()).thenReturn(factIdList);
        mockMvc.perform(MockMvcRequestBuilders.get("/facts").contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[0]").value(factIdList.get(0)))
                .andExpect(jsonPath("$.[1]").value(factIdList.get(1))).andDo(print())
                .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    @WithMockUser
    void getRandomFact_ValidId() throws Exception {

        Fact fact = FactTestDataFactory.getDefaultFact();

        when(factService.getById(fact.getId())).thenReturn(fact);
        mockMvc.perform(MockMvcRequestBuilders.get("/facts/" + fact.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(fact.getId())))
                .andExpect(jsonPath("$.text", is(fact.getText())))
                .andExpect(jsonPath("$.url", is(fact.getUrl())))
                .andExpect(jsonPath("$.language", is(fact.getLanguage()))).andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    @WithMockUser
    void getRandomFact_InvalidId() throws Exception {

        Fact fact = FactTestDataFactory.getDefaultFact();

        when(factService.getById(fact.getId())).thenThrow(new RandomFactNotFoundException());
        mockMvc.perform(MockMvcRequestBuilders.get("/facts/" + fact.getId()).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(equalTo("Fact not downloaded.")));

    }


}