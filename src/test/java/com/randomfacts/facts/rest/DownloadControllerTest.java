package com.randomfacts.facts.rest;

import com.randomfacts.facts.TestConfig;
import com.randomfacts.facts.domain.FactTotal;
import com.randomfacts.facts.domain.enums.DownloadStatus;
import com.randomfacts.facts.domain.DownloadStatusDetails;
import com.randomfacts.facts.service.FactDownloadService;
import com.randomfacts.facts.service.FactService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest
@Import(TestConfig.class)
class DownloadControllerTest {

    @Autowired
    private
    MockMvc mockMvc;

    @MockBean
    private FactDownloadService factDownloadService;

    @MockBean
    private FactService factService;

    private int total = 1000;
    private int unique = 256;

    @Test
    @WithMockUser
    void getDownloadStatus_IsCompleted() throws Exception {
        when(factDownloadService.getDownloadStatusDetails()).thenReturn(new DownloadStatusDetails(DownloadStatus.COMPLETED, new FactTotal(total, unique)));

        mockMvc.perform(MockMvcRequestBuilders.get("/status").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(DownloadStatus.COMPLETED.name())))
                .andExpect(jsonPath("$.facts.total", is(total)))
                .andExpect(jsonPath("$.facts.unique", is(unique)))
                .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    @WithMockUser
    void getDownloadStatus_IsStillLoading() throws Exception {
        when(factDownloadService.getDownloadStatusDetails()).thenReturn(new DownloadStatusDetails(DownloadStatus.IN_PROGRESS, new FactTotal(total, unique)));

        mockMvc.perform(MockMvcRequestBuilders.get("/status/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(DownloadStatus.IN_PROGRESS.name())))
                .andExpect(jsonPath("$.facts.total").exists())
                .andExpect(jsonPath("$.facts.unique").exists())
                .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    @WithMockUser
    void getDownloadStatus_ErrorOccurred() throws Exception {
        when(factDownloadService.getDownloadStatusDetails()).thenReturn(new DownloadStatusDetails(DownloadStatus.ERROR_OCCURRED, new FactTotal(total, unique)));

        mockMvc.perform(MockMvcRequestBuilders.get("/status/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(DownloadStatus.ERROR_OCCURRED.name())))
                .andExpect(jsonPath("$.facts.total").exists())
                .andExpect(jsonPath("$.facts.unique").exists())
                .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    @WithMockUser
    void getDownloadStatus_ReadyToStart() throws Exception {
        when(factDownloadService.getDownloadStatusDetails()).thenReturn(new DownloadStatusDetails(DownloadStatus.READY_TO_START, new FactTotal(total, unique)));

        mockMvc.perform(MockMvcRequestBuilders.get("/status/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(DownloadStatus.READY_TO_START.name())))
                .andExpect(jsonPath("$.facts.total").exists())
                .andExpect(jsonPath("$.facts.unique").exists())
                .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
}
