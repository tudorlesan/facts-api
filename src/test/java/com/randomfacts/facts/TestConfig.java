package com.randomfacts.facts;

import com.randomfacts.facts.service.JwtUserDetailsService;
import com.randomfacts.facts.utils.JwtRequestFilter;
import com.randomfacts.facts.utils.JwtUtil;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration()
public class TestConfig {
    @Bean
    JwtUserDetailsService getJwtUserDetailsService() {
        return new JwtUserDetailsService();
    }

    @Bean
    JwtUtil getJwtTokenUtil() {
        return new JwtUtil();
    }

    @Bean
    JwtRequestFilter requestFilter() { return new JwtRequestFilter();}

}
