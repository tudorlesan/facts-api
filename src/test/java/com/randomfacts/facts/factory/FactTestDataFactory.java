package com.randomfacts.facts.factory;

import com.randomfacts.facts.domain.Fact;
import com.randomfacts.facts.domain.enums.Language;

import java.util.Arrays;
import java.util.List;

public class FactTestDataFactory {

    public static Fact getDefaultFact(){
        return new Fact("295d651b-5c31-441c-a36d-03131d578ec8",
                "Ninety percent of all species that have become extinct have been birds.",
                "https://uselessfacts.jsph.pl/295d651b-5c31-441c-a36d-03131d578ec8",
                Language.ENGLISH);
    }

    public static Fact getAlternateFact(){
        return new Fact("7e7f1cd1-d1fa-4fae-9d89-d24cad244c5c",
                "Bier galt bis Sommer 2011 in Russland nicht als alkoholisches Getränk",
                "https://uselessfacts.jsph.pl/7e7f1cd1-d1fa-4fae-9d89-d24cad244c5c",
                Language.GERMAN);
    }

    public static List<Fact> getFactList() {
        return Arrays.asList(getDefaultFact(), getAlternateFact());
    }
}
