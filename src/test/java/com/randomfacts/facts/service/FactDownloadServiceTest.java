package com.randomfacts.facts.service;

import com.randomfacts.facts.domain.FactTotal;
import com.randomfacts.facts.domain.enums.DownloadStatus;
import com.randomfacts.facts.domain.DownloadStatusDetails;
import com.randomfacts.facts.domain.Fact;
import com.randomfacts.facts.factory.FactTestDataFactory;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorService;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class FactDownloadServiceTest {

    @Mock
    RestTemplate restTemplate;

    @Mock
    ExecutorService executorService;

    @Mock
    CompletionService<Fact> completionService;

    private String endpoint = "http://dummy.url";
    private int totalRequests = 10;
    private int totalUnique = 1;

    @InjectMocks
    private FactDownloadService factDownloadService;

    @BeforeEach
    void setUp() throws NoSuchFieldException, InterruptedException {
        FieldSetter.setField(factDownloadService, factDownloadService.getClass().getDeclaredField("downloadExecutor"), executorService);
        FieldSetter.setField(factDownloadService, factDownloadService.getClass().getDeclaredField("downloadCompletionService"), completionService);

        when(completionService.take()).thenReturn(CompletableFuture.completedFuture(FactTestDataFactory.getDefaultFact()));
    }


    @Test
    void getDownloadStatus_IsReady() throws InterruptedException, NoSuchFieldException {
        FieldSetter.setField(factDownloadService, factDownloadService.getClass().getDeclaredField("downloadStatusDetails"), new DownloadStatusDetails());

        factDownloadService.start(endpoint, totalRequests);

        assertEquals(DownloadStatus.COMPLETED, factDownloadService.getDownloadStatusDetails().getDownloadStatus());
        assertEquals(totalRequests, factDownloadService.getDownloadStatusDetails().getFactTotal().getTotal());
        assertEquals(totalUnique, factDownloadService.getDownloadStatusDetails().getFactTotal().getUnique());
        Assert.assertThat(factDownloadService.getDownloadStatusDetails().getFactTotal().getTotal(), greaterThanOrEqualTo(factDownloadService.getDownloadStatusDetails().getFactTotal().getUnique()));
        verify(completionService, times(totalRequests)).submit(any());
        verify(completionService, times(totalRequests)).take();
    }

    @Test
    void getDownloadStatus_ErrorOccuredThenUniqueAdds() throws InterruptedException, NoSuchFieldException {
        FieldSetter.setField(factDownloadService, factDownloadService.getClass().getDeclaredField("downloadStatusDetails"),
                new DownloadStatusDetails(DownloadStatus.ERROR_OCCURRED, new FactTotal(1, 1)));

        int totalRequests = 2;

        when(completionService.take()).thenAnswer(new Answer() {
            private int count = 0;

            public CompletableFuture<Fact> answer(InvocationOnMock invocation) {
                if (count++ == 1)
                    return CompletableFuture.completedFuture(FactTestDataFactory.getDefaultFact());
                return CompletableFuture.completedFuture(FactTestDataFactory.getAlternateFact());
            }
        });

        factDownloadService.start(endpoint, 2);

        assertEquals(DownloadStatus.COMPLETED, factDownloadService.getDownloadStatusDetails().getDownloadStatus());
        assertEquals(totalRequests, factDownloadService.getDownloadStatusDetails().getFactTotal().getTotal());
        assertEquals(totalRequests, factDownloadService.getDownloadStatusDetails().getFactTotal().getUnique());
        Assert.assertThat(factDownloadService.getDownloadStatusDetails().getFactTotal().getTotal(), greaterThanOrEqualTo(factDownloadService.getDownloadStatusDetails().getFactTotal().getUnique()));
        verify(completionService, times(totalRequests)).submit(any());
        verify(completionService, times(totalRequests)).take();
    }


    @Test
    void getDownloadStatus_IsInProgress() throws InterruptedException, NoSuchFieldException {
        FieldSetter.setField(factDownloadService, factDownloadService.getClass().getDeclaredField("downloadStatusDetails"),
                new DownloadStatusDetails(DownloadStatus.IN_PROGRESS, new FactTotal(1, 1)));

        factDownloadService.start(endpoint, totalRequests);

        assertEquals(DownloadStatus.IN_PROGRESS, factDownloadService.getDownloadStatusDetails().getDownloadStatus());
        verify(completionService, never()).submit(any());
        verify(completionService, never()).take();
    }

    @Test
    void getDownloadStatus_RestartThenErrorOccurred() throws NoSuchFieldException, InterruptedException {
        FieldSetter.setField(factDownloadService, factDownloadService.getClass().getDeclaredField("failedFacts"), 1);
        FieldSetter.setField(factDownloadService, factDownloadService.getClass().getDeclaredField("downloadStatusDetails"),
                new DownloadStatusDetails(DownloadStatus.COMPLETED, new FactTotal(1, 1)));

        factDownloadService.start(endpoint, totalRequests);

        assertEquals(DownloadStatus.ERROR_OCCURRED, factDownloadService.getDownloadStatusDetails().getDownloadStatus());
    }

}
