package com.randomfacts.facts.service;

import com.randomfacts.facts.domain.FactTotal;
import com.randomfacts.facts.domain.enums.DownloadStatus;
import com.randomfacts.facts.domain.DownloadStatusDetails;
import com.randomfacts.facts.domain.Fact;
import com.randomfacts.facts.factory.FactTestDataFactory;
import com.randomfacts.facts.utils.RandomFactNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class FactServiceTest {

    @Mock
    private FactDownloadService factDownloadService;

    @InjectMocks
    private FactService factService;

    private List<Fact> factList = FactTestDataFactory.getFactList();

    @BeforeEach
    void setUp() {
        when(factDownloadService.getDownloadStatusDetails()).thenReturn(new DownloadStatusDetails(DownloadStatus.READY_TO_START, new FactTotal(0, 0)));
        when(factDownloadService.getAll()).thenReturn(factList);
        when(factDownloadService.getById(anyString())).thenReturn(FactTestDataFactory.getDefaultFact());
    }

    @Test
    void getAllRandomFacts_DownloadInProgress() {
        when(factDownloadService.getDownloadStatusDetails()).thenReturn(new DownloadStatusDetails(DownloadStatus.IN_PROGRESS, new FactTotal(1, 1)));

        List<String> randomFactList = factService.getAllIds();

        assertEquals(factList.size(), randomFactList.size());
        assertEquals(factList.get(0), FactTestDataFactory.getDefaultFact());
        assertEquals(factList.get(1), FactTestDataFactory.getAlternateFact());
    }


    @Test
    void getAllRandomFacts_DownloadReadyToStart() {
        when(factDownloadService.getDownloadStatusDetails()).thenReturn(new DownloadStatusDetails(DownloadStatus.READY_TO_START, new FactTotal(0, 0)));

        List<String> randomFactList = factService.getAllIds();

        verify(factDownloadService).start(any(), anyInt());
        assertEquals(factList.size(), randomFactList.size());
        assertEquals(factList.get(0), FactTestDataFactory.getDefaultFact());
        assertEquals(factList.get(1), FactTestDataFactory.getAlternateFact());
    }


    @Test
    void getRandomFact_ValidObject() throws RandomFactNotFoundException {
        Fact fact = FactTestDataFactory.getDefaultFact();

        Fact returnedFact = factService.getById(fact.getId());

        verify(factDownloadService).getById(fact.getId());
        assertEquals(fact, returnedFact);
    }

}
