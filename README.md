# Facts API

An API for retrieving random useless facts

Developed by Tudor Lesan using Java 11 and Spring Boot

## Features
* Download specified number of facts
* Retrieve fact based on ID
* Retrieve and translate fact based on ID
* In-memory storage
* Stand-alone jar (no need for a pre-installed container/server)
* Swagger UI can be found here: (http://localhost:8080/swagger-ui.html)

## Should be added/improved
* Security: it is just POC level, e.g. User details should be stored in DB, should use different PasswordEncoder, etc.
* Tests: TDD was done but ultimately was dropped in favour of development speed. Test coverage should be higher and also Integration tests should be implemented to ensure the external APIs are up and returning expected responses.
* Monitoring: Ensure logs contain all needed information. Metrics and health checks (Actuator)
* Performance: Ensure thread pool size is in the sweetspot. Ensure testing environment does not load unnecessary beans.
* Miscellaneous: More descriptive exception handling. Dev/Prod profiles and corresponding log levels. Retrieving of Facts doesn't have to be blocking as each object could be added asynchronously to a concurrent collection. Swagger documentation can be more descriptive. Various scalability improvements depending on further requirements.
* Possibly others that have skipped my mind...

## Requirements
* Java 11
* Maven

## How to build the application
Checkout the project from this repository, then run
```
    mvn clean package (can use wrappers mvnw/mvnw.cmd or use directly the [Application jar](https://drive.google.com/open?id=11gd7FBW9KW_1vi3_eqa-7BMVGZBxuz99))
```
Or use directly the [Application jar](https://drive.google.com/open?id=11gd7FBW9KW_1vi3_eqa-7BMVGZBxuz99)
## How to run tests
Checkout the project from this repository, then run
```
    mvn clean test (can use wrappers mvnw/mvnw.cmd)
```
## How to run the application
Build the application then run
```
    java -jar target\facts-api-0.0.1-SNAPSHOT.jar --yandex.api.key="<API_KEY>" --jwt.secret="<JWT_SECRET>"
```
The application runs on port 8080

## How to use the application
## Swagger
One can test the API methods using Swagger instead of an Http Client like Postman.
The method /authenticate must first be called with correct username and password. A JWT is then returned which can be used in the "Authorize" popup triggered
by the buttom above the Swagger Controller documentation.
In the "Value:" field input - "Bearer " followed by your retrieved JWT. This will permit all further requests.

## Authenticate
### Retrieve a JWT Token
The following request authenticates a user and generates a JWT Token
```
    Content-Type - Application/json
    POST localhost:8080/authenticate
    {
        "username":"user"
        "password":"pass"
    }
```
Response:
```
{
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiZXhfdwIjoxNTgwNjczMDgfdgf3543mDaIM_yDCZzHbb43nOuI9rUV4ocUUfpY8gZs6uIO7HY4UvdLJJNig7nptQpyQ"
}
```
The token above is not valid.
### Facts
#### Start downloading facts
The following request triggers a download process from (https://uselessfacts.jsph.pl/random.json) as URL mentioned
in the requirements mentioned the API was moved. Number of objects to be downloaded can be specified through environment variable.
```
    Authorization - Bearer <PREVIOUSLY_OBTAINED_JWT>
    GET localhost:8080/facts
```
Response:
```
    HTTP 200 OK
    [
      "e1993665-1fed-4360-b012-d62e690427e1",
      "4180f1f8-0a61-4fe1-ba59-53e5a130d68d",
      "f50a2300-c16d-4b91-88f3-a3111de022c4",
      "f1ea6706-60de-48a4-89ce-186065abd159",
      "d2449b86-0e4e-4e2e-8cbe-f8a3ee96b3d3",
      "d772e955-1740-419b-9623-51c2216768e0",
       ...
     ]
```
#### Retrieve a fact based on ID
The following request returns a fact which lies in the cache.
```
    Authorization - Bearer <PREVIOUSLY_OBTAINED_JWT>
    GET localhost:8080/e1993665-1fed-4360-b012-d62e690427e1
```
Response:
```
    HTTP 200 OK
    {
        "id": "d2449b86-0e4e-4e2e-8cbe-f8a3ee96b3d3",
        "text": "Wale sprechen Dialekt",
        "language": "de",
        "url": "https://uselessfacts.jsph.pl/d2449b86-0e4e-4e2e-8cbe-f8a3ee96b3d3"
    }
```
* If the requested ID is not the the cache FactNotFoundException is thrown

#### Retrieve a translated fact based on ID
The following request returns a translated fact which lies in the cache.
```
    Authorization - Bearer <PREVIOUSLY_OBTAINED_JWT>
    GET localhost:8080/e1993665-1fed-4360-b012-d62e690427e1?lang=en
```
Response:
```
    HTTP 200 OK
    {
        "id": "d2449b86-0e4e-4e2e-8cbe-f8a3ee96b3d3",
        "text": "Whales speak in dialect",
        "language": "en",
        "url": "https://uselessfacts.jsph.pl/d2449b86-0e4e-4e2e-8cbe-f8a3ee96b3d3"
    }
```
Languages:
* {ENGLISH("english", "en"), GERMAN("german", "de"), FRENCH("french", "fr"), ITALIAN("italian", "it"), SPANISH("spanish", "es"), RUSSIAN("russian", "ru");
* If the requested language is not supported, the original fact is retrieved.

#### Retrieve the download status
The folowing request retrieves the download status
```
    Authorization - Bearer <PREVIOUSLY_OBTAINED_JWT>
    GET localhost:8080/status
```
Response:
```
    HTTP 200 OK
{
    "status": "COMPLETED",
    "facts": {
        "total": 1000,
        "unique": 641
    }
}
```
Status types:
{COMPLETED, IN_PROGRESS, READY_TO_START, ERROR_OCCURRED}
